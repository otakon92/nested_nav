import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NestedPage1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Ciao sono la pagina innestata 1",
            ),
            ElevatedButton(
              child: Text("Vai alla pagina innestata 2"),
              onPressed: () => Navigator.of(context).pushNamed('NestedPage2'),
            ),
          ],
        ),
      ),
    );
  }
}

class NestedPage2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Ciao sono la pagina innestata 2",
            ),
            ElevatedButton(
              child: Text("Vai alla pagina innestata 4 a schermo intero"),
              onPressed: () => Navigator.of(context, rootNavigator: true).push(
                MaterialPageRoute(
                  builder: (_) => NestedPage4(),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NestedPage3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Ciao sono la pagina innestata 3",
            ),
          ],
        ),
      ),
    );
  }
}

class NestedPage4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: BackButton(),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Ciao sono la pagina innestata 4",
            ),
          ],
        ),
      ),
    );
  }
}

class BackButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () => Navigator.of(context).pop(),
      icon: Icon(CupertinoIcons.chevron_back),
    );
  }
}
