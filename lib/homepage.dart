import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:nested_nav/nested_pages.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin {
  late TabController tabController;
  late List<Widget> mainTabs = [];

  @override
  void initState() {
    super.initState();
    tabController = TabController(
      length: 2,
      vsync: this,
    );
    mainTabs = [tab1, tab2];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: TabBarView(
        controller: tabController,
        children: mainTabs,
      ),
      bottomNavigationBar: BottomAppBar(
        child: TabBar(
          controller: tabController,
          tabs: [
            Tab(
              text: "Pagina 1",
              icon: Icon(
                CupertinoIcons.number,
                color: Colors.blueAccent,
              ),
            ),
            Tab(
              text: "Pagina 2",
              icon: Icon(
                CupertinoIcons.list_bullet_below_rectangle,
                color: Colors.blueAccent,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget get tab1 {
    return Navigator(
      initialRoute: 'NestedPage1',
      onGenerateRoute: (RouteSettings settings) {
        Widget toReturn = Container();
        switch (settings.name) {
          case 'NestedPage1':
            toReturn = NestedPage1();
            break;
          case 'NestedPage2':
            toReturn = NestedPage2();
            break;
          default:
            break;
        }
        return MaterialPageRoute(
          builder: (BuildContext context) {
            return toReturn;
          },
        );
      },
    );
  }

  Widget get tab2 {
    return Navigator(
      initialRoute: 'NestedPage3',
      onGenerateRoute: (RouteSettings settings) {
        Widget toReturn = Container();
        switch (settings.name) {
          case 'NestedPage3':
            toReturn = NestedPage3();
            break;
          default:
            break;
        }
        return MaterialPageRoute(
          builder: (BuildContext context) {
            return toReturn;
          },
        );
      },
    );
  }
}
